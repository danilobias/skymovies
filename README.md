# Cine SKY

Aplicativo para exibição de filmes;

## Biblioteca utilizadas:
- [Alamofire](https://github.com/Alamofire/Alamofire)
- [Kingfisher](https://github.com/onevcat/Kingfisher)
- [SwiftyJSON](https://github.com/SwiftyJSON/SwiftyJSON)
- [SVProgressHUD](https://github.com/SVProgressHUD/SVProgressHUD)

# Instalação

Para executar o projeto, é necessário ter o Cocoapods instalado:

- sudo gem install cocoapods
- pod install
- Abrir o projeto através do arquivo Movies.xcworkspace

## Requisitos:

- iOS 11.0+
- Xcode 9.4.1+
- Swift 4.1

### TODO

 - Testes automatizados;


### Contato
- [LinkedIn](https://www.linkedin.com/in/danilobias/)

- E-mail:(<danilobias@hotmail.com> / <danilob.lago@gmail.com>).
