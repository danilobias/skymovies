//
//  Constants.swift
//  Movies
//
//  Created by Danilo Bias Lago on 18/09/2018.
//  Copyright © 2018 Danilo Bias Lago. All rights reserved.
//

import UIKit

struct Constants {
    
    //MARK: URL's e métodos
    struct APIPreffix {
        static let urlPreffix: String = "https://sky-exercise.herokuapp.com/api/"
    }
    
    struct MoviesMethods {
        static let getMovies = "/Movies"
    }
    
    struct APIUrls {
        static let getMoviesUrl = Constants.APIPreffix.urlPreffix + MoviesMethods.getMovies
    }
}
