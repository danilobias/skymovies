//
//  ErrorManager.swift
//  Movies
//
//  Created by Danilo Bias Lago on 18/09/2018.
//  Copyright © 2018 Danilo Bias Lago. All rights reserved.
//

import UIKit

struct ErrorManager {
    static func error(type: ErrorType) -> NSError {
        switch type {
        case .unknown:
            return NSError(domain: "Não foi possível acessar no momento.\nTente novamente, por favor", code: 404, userInfo: nil)
        case .alamofire:
            return NSError(domain: "Ocorreu um erro inesperado.\nTente novamente, por favor.", code: 430, userInfo: nil)
        }
    }
}
