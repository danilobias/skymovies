//
//  Enums.swift
//  Movies
//
//  Created by Danilo Bias Lago on 18/09/2018.
//  Copyright © 2018 Danilo Bias Lago. All rights reserved.
//

import UIKit

enum ErrorType {
    case unknown
    case alamofire
}
