//
//  BaseRequest.swift
//  Movies
//
//  Created by Danilo Bias Lago on 18/09/2018.
//  Copyright © 2018 Danilo Bias Lago. All rights reserved.
//

import UIKit
import Alamofire

class BaseRequest: NSObject {
    
    static let timeout: TimeInterval = 7.0
    
    static let basicHeaders = [
        "accept-language": "en",
        "cache-control": "no-cache"
    ]
    
    static var manager: SessionManager {
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = timeout
        return manager
    }
    
    static func get(_ url: String, _ parameters: Dictionary<String, Any>? = nil,  completion: @escaping(Any?) -> Void) {
        request(url.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) ?? url, method: .get, parameters: parameters, encoding: JSONEncoding.default, headers: basicHeaders).validate(statusCode: 200..<300).responseJSON { (response) in
            
            switch response.result{
            case .success:
                completion(response.data)
                break
                
            case .failure(let error):
                completion(error)
                break
            }
        }
    }
}
