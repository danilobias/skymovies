//
//  MoviesRequest.swift
//  Movies
//
//  Created by Danilo Bias Lago on 18/09/2018.
//  Copyright © 2018 Danilo Bias Lago. All rights reserved.
//

import UIKit
import SwiftyJSON

class MoviesRequest: NSObject {

    static func getMovies(withURL url: String, completion: @escaping([Movie]?, Error?) -> Void) {
        BaseRequest.get(url) { (result) in
            if let data = result as? Data {
                
                let json: JSON = JSON(data)
                var movies: [Movie] = []
                
                for (_, subJson):(String, JSON) in json {
                    let movie: Movie = Movie(json: subJson)
                    movies.append(movie)
                }
                
                completion(movies, nil)
                
            }else if let error = result as? Error {
                completion(nil, error)
            }else{
                completion(nil, ErrorManager.error(type: .unknown))
            }
        }
    }
}
