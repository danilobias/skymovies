//
//  MovieCollectionViewCell.swift
//  Movies
//
//  Created by Danilo Bias Lago on 18/09/2018.
//  Copyright © 2018 Danilo Bias Lago. All rights reserved.
//

import UIKit
import Kingfisher

class MovieCollectionViewCell: UICollectionViewCell {
    
    // MARK: - IBOutlets
    @IBOutlet weak var movieImageView: UIImageView!
    @IBOutlet weak var movieNameLabel: UILabel!
    
    // MARK: - Methods
    override func prepareForReuse() {
        super.prepareForReuse()
        self.movieImageView.kf.cancelDownloadTask()
        self.movieImageView.image = UIImage(named: "ic_claquete_big")!
        self.movieNameLabel.text = " - "
    }
    
    // MARK: - Layout configs
    func configCellWith(movie: Movie) {
        
        self.movieNameLabel.text = movie.title
        if let _url = movie.coverUrl {
            self.loadImage(urlString: _url)
        }
    }
    
    func loadImage(urlString: String) {
        if let url = URL(string: urlString) {
            let placeholderImage = UIImage(named: "ic_claquete_big")!
            self.movieImageView.kf.setImage(with: url, placeholder: placeholderImage) {  (image, error, cacheType, url) in
                self.setNeedsLayout()
            }
        }
    }
}
