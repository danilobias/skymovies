//
//  SectionHeader.swift
//  Movies
//
//  Created by Danilo Bias Lago on 19/09/2018.
//  Copyright © 2018 Danilo Bias Lago. All rights reserved.
//

import UIKit

class SectionHeader: UICollectionReusableView {
    @IBOutlet weak var sectionHeaderlabel: UILabel!
}
