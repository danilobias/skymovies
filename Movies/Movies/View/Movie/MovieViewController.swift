//
//  MovieViewController.swift
//  Movies
//
//  Created by Danilo Bias Lago on 18/09/2018.
//  Copyright © 2018 Danilo Bias Lago. All rights reserved.
//

import UIKit

class MovieViewController: BaseViewController {

    // MARK: - IBOutlets
    @IBOutlet weak var moviesCollectionView: UICollectionView!
    
    // MARK: - Lets and Vars
    var moviesViewModel: MoviesViewModel! {
        didSet {
            moviesViewModel.responseDidChange = { [weak self] viewModel in
                self?.finishGetMovies()
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()    
        self.moviesViewModel = MoviesViewModel()
        self.makeMoviesRequest()
    }

    // MARK: - Requests
    func makeMoviesRequest() {
        self.showLoading()
        self.moviesViewModel.getElement(completion: { (error) in
            // TO-DO: Tratar erro
            self.hideLoading()
        })
    }
    
    func finishGetMovies() {
        self.hideLoading {
            self.moviesCollectionView.reloadData()
        }
    }
    
    // MARK: - Utils
    func getMovieBy(index: Int) -> Movie {
        return self.moviesViewModel!.getMoviesBy(index: index)
    }

    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }

    // MARK: - Memory
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension MovieViewController: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.moviesViewModel.numberOfRows()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: MovieCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "MovieCollectionViewCell", for: indexPath) as! MovieCollectionViewCell
        let movie: Movie = self.getMovieBy(index: indexPath.row)
        cell.configCellWith(movie: movie)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        if let sectionHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "SectionHeaderIdentifier", for: indexPath) as? SectionHeader{
            return sectionHeader
        }
        return UICollectionReusableView()
    }
}

extension MovieViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let itemsPerRow: CGFloat = 2
        let hardCodedPadding: CGFloat = 10
        let itemWidth: CGFloat = (collectionView.bounds.width / itemsPerRow) - hardCodedPadding
        let itemHeight: CGFloat = 318.0
        return CGSize(width: itemWidth, height: itemHeight)
    }
}
