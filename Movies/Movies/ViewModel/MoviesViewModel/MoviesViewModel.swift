//
//  MoviesViewModel.swift
//  Movies
//
//  Created by Danilo Bias Lago on 18/09/2018.
//  Copyright © 2018 Danilo Bias Lago. All rights reserved.
//

import UIKit

protocol MoviesViewModelProtocol: ListProtocol {
    var movies: [Movie] { get }
    var responseDidChange: ((MoviesViewModelProtocol) -> Void)? { get set }
}

class MoviesViewModel: MoviesViewModelProtocol {
    
    // MARK: - Vars
    var movies: [Movie] {
        didSet{
            self.responseDidChange?(self)
        }
    }
    
    var url: String = Constants.APIUrls.getMoviesUrl
    var responseDidChange: ((MoviesViewModelProtocol) -> Void)?
    
    // MARK: - Methods
    required init() {
        self.movies = []
    }
    
    // MARK: - Utils
    func numberOfRows() -> Int{
        return self.movies.count
    }
    
    func getMoviesBy(index: Int) -> Movie {
        return self.movies[index]
    }
    
    
    // MARK: - Request
    func getElement(completion: @escaping (Error?) -> Void) {
        
        MoviesRequest.getMovies(withURL: url) { (moviesResponse, error) in
            if let allMovies = moviesResponse {
                self.movies = allMovies
            }
            
            if let errorDetail = error {
                completion(errorDetail)
            }
        }
    }
}
