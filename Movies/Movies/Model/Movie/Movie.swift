//
//  Movie.swift
//
//  Created by Danilo Bias Lago on 18/09/2018
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public class Movie {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let overview = "overview"
        static let id = "id"
        static let coverUrl = "cover_url"
        static let title = "title"
        static let backdropsUrl = "backdrops_url"
        static let releaseYear = "release_year"
        static let duration = "duration"
    }
    
    // MARK: Properties
    public var overview: String?
    public var id: String?
    public var coverUrl: String?
    public var title: String?
    public var backdropsUrl: [String]?
    public var releaseYear: String?
    public var duration: String?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        overview = json[SerializationKeys.overview].string
        id = json[SerializationKeys.id].string
        coverUrl = json[SerializationKeys.coverUrl].string
        title = json[SerializationKeys.title].string
        if let items = json[SerializationKeys.backdropsUrl].array { backdropsUrl = items.map { $0.stringValue } }
        releaseYear = json[SerializationKeys.releaseYear].string
        duration = json[SerializationKeys.duration].string
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = overview { dictionary[SerializationKeys.overview] = value }
        if let value = id { dictionary[SerializationKeys.id] = value }
        if let value = coverUrl { dictionary[SerializationKeys.coverUrl] = value }
        if let value = title { dictionary[SerializationKeys.title] = value }
        if let value = backdropsUrl { dictionary[SerializationKeys.backdropsUrl] = value }
        if let value = releaseYear { dictionary[SerializationKeys.releaseYear] = value }
        if let value = duration { dictionary[SerializationKeys.duration] = value }
        return dictionary
    }
    
}
